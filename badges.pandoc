% ----------------------------------------------------------------------------
% Pandoc Template for Badges in LaTeX.
% Copyright (c) 2019 - Romain Wallon.
%
% This work is licensed under a Creative Commons Attribution 4.0 International
% License (https://creativecommons.org/licenses/by/4.0/).
% ----------------------------------------------------------------------------

% -------------------------- DOCUMENT CONFIGURATION --------------------------

\documentclass[a4paper, 10pt]{letter}

% ----------------------------- PACKAGE LOADING ------------------------------

\usepackage[nomessages]{fp}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage[freepin, boxed]{ticket}
\usepackage[dvipsnames]{xcolor}

% --------------------------- COLOR CONFIGURATION ----------------------------

$for(color)$
\definecolor{$color.name$}{$color.type$}{$color.value$}
$endfor$

% --------------------------- BADGE CONFIGURATION ----------------------------

% Setting millimeter as unit.
\setlength{\unitlength}{1mm}

% Computing the position of the various elements.
\FPeval{\centerposition}{$width$/2}
\FPeval{\headerposition}{9*$height$/10}
\FPeval{\nameposition}{2*$height$/3}
\FPeval{\instituteposition}{$height$/2}
\FPeval{\footlineposition}{$height$/4}
\FPeval{\footlinewidth}{$width$-10}
\FPeval{\logoposition}{1.75*$height$/16}
\FPeval{\firstfooterposition}{2.5*$height$/16}
\FPeval{\secondfooterposition}{$height$/16}

% Specifying the layout for the badges.
\ticketSize{$width$}{$height$}
\ticketDistance{0}{0}
\ticketNumbers{$if(columns)$$columns$$else$2$endif$}{$if(rows)$$rows$$else$4$endif$}

% ----------------------------- BADGE APPEARANCE -----------------------------

% The part common to all the badges.
\renewcommand{\ticketdefault}{
    \put (5, \footlineposition) {
        $if(line-color)$\color{$line-color$}$endif$
        \line (1, 0) {\footlinewidth}
    }

$if(logo)$
    \put (\centerposition, \headerposition) {
        \makebox (0, 0) {
            $if(event-color)$\color{$event-color$}$endif$
            \scriptsize\textsc{$event$ -- $location$, $date$}
        }
    }

    \put (0, \logoposition) {
        \makebox ($width$, 0) {
            \hfill
$for(logo)$
            \includegraphics[height=$if(logo-height)$$logo-height$$else$7mm$endif$]{$logo$}$sep$\hfill
$endfor$
            \hfill
        }
    }
$else$
    \put (7, \firstfooterposition) {
        $if(event-color)$\color{$event-color$}$endif$
        \scriptsize\textsc{$event$}
    }

    \put (7, \secondfooterposition) {
        $if(event-color)$\color{$event-color$}$endif$
        \scriptsize\textsc{$location$, $date$}
    }
$endif$
}

% The appearance of the main content.
\newcommand{\badge}[2]{
    \ticket{
        \put (\centerposition, \nameposition) {
            \makebox (0, 0) {
                $if(name-color)$\color{$name-color$}$endif$
                \centering\bfseries\huge #1
            }
        }

        \put (\centerposition, \instituteposition) {
            \makebox (0, 0) {
                $if(institute-color)$\color{$institute-color$}$endif$
                \centering\bfseries\Large #2
            }
        }
    }
}

% ----------------------------- DOCUMENT CONTENT -----------------------------

\begin{document}
    \sffamily

$for(participants)$
    \badge{$participants.name$}{$participants.institute$}
$endfor$
\end{document}
