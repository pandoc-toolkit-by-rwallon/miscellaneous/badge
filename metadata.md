---
# Color Definitions.
color:
    - name:
      type:
      value:

# Color Settings.
name-color:
institute-color:
event-color:
line-color:

# Badge Layout.
width:
height:
columns:
rows:

# Informations about the Event.
event:
location:
date:

# Logos of the Event.
logo:
    -
logo-height:

# Informations about the Participants.
participants:
    - name:
      institute:
---
