---
# Color Settings.
name-color: BrickRed
institute-color: CarnationPink
event-color: Fuchsia
line-color: Brown

# Badge Layout.
width: 90
height: 57
columns: 2
rows: 4

# Informations about the Event.
event: Release 0.1.0
location: Pandoc Toolkit
date: December 2019

# Logos of the Event.
logo:
    - logos/latex.png
    - logos/markdown.png

# Informations about the Participants.
participants:
    - name: Hugh Jass
      institute: University of Pandoc
    - name: Amanda Hugginkiss
      institute: Markdown School
    - name: Phil McCrackin
      institute: LaTeX Institute
    - name: Isabelle Ringing
      institute: University of Pandoc
    - name: Dev Null
      institute: Markdown School
    - name: Maya Buttreeks
      institute: LaTeX Institute
    - name: Cowboy Neal
      institute: University of Pandoc
    - name: Anita Bath
      institute: Markdown School
---
