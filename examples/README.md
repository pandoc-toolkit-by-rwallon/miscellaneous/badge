# Use Cases of the *Badge* Template

This directory contains examples illustrating different use cases of the Pandoc
template for creating badges.

For each of the examples, you may either read its metadata file (to see how we
configured it) or download the final PDF.

## Badges Without Logos

The configuration of these badges does not use any logo.

*Metadata file available [here](no-logo.md).*

*PDF available [here](/../builds/artifacts/master/file/no-logo.pdf?job=make-example).*

## Badges With Logos

The logos of the event are displayed on these badges.

*Metadata file available [here](with-logos.md).*

*PDF available [here](/../builds/artifacts/master/file/with-logos.pdf?job=make-example).*
