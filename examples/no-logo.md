---
# Color Settings.
name-color: Blue
institute-color: Aquamarine
event-color: Cerulean
line-color: OrangeRed

# Badge Layout.
width: 80
height: 40
columns: 2
rows: 5

# Informations about the Event.
event: Release 0.1.0
location: Pandoc Toolkit
date: December 2019

# Informations about the Participants.
participants:
    - name: Hugh Jass
      institute: University of Pandoc
    - name: Amanda Hugginkiss
      institute: Markdown School
    - name: Phil McCrackin
      institute: LaTeX Institute
    - name: Isabelle Ringing
      institute: University of Pandoc
    - name: Dev Null
      institute: Markdown School
    - name: Maya Buttreeks
      institute: LaTeX Institute
    - name: Cowboy Neal
      institute: University of Pandoc
    - name: Anita Bath
      institute: Markdown School
    - name: Mike Hunt
      institute: LaTeX Institute
    - name: Helen Bedd
      institute: University of Pandoc
---
