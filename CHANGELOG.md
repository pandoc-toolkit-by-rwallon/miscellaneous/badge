# Changelog

This file describes the evolution of the *Badge* LaTeX template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.1.0 (December 2019)

+ Badge appearance can be modified from YAML.
+ Names and institutes can be specified from YAML.
+ Informations about the event can be specified from YAML.
