# Pandoc Template for *Badges*

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/badge/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/badge/commits/master)

## Description

This project provides a template allowing an easy creation of badges with
[Pandoc](https://pandoc.org).

This template is distributed under a Creative Commons Attribution 4.0
International License.

[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)

## Requirements

To build your badges using this template, [Pandoc](https://pandoc.org) at least
2.0 have to be installed on your computer.

You also need to have the class
[`ticket`](https://www.ctan.org/tex-archive/macros/latex/contrib/ticket)
installed in your [LaTeX](https://www.latex-project.org) distribution.

## Creating your Badges

To create your badges, you need to setup their YAML configuration.
For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones which do not fit your needs.
Read below for details on how to configure your badges, and see our various
[examples](examples).

### Badge Content

You may specify the informations about the event in which the badges are worn
using the following variables:

+ `event`: the name of the event.
+ `location`: the location in which the event takes place.
+ `date`: the date of the event.

If you have any logo associated to the event, you may specify them in the list
`logo`.
If you need to modify their size, you may specify it with `logo-height`.

The list `participants` allows to specify, for all participants taking part in
the event, their `name` and `institute` (if any).

### Badge Layout

You may specify the `width` and `height` of your badges using the corresponding
variables.
They must be set in **millimeters**.

You may also specify in how many `rows` and `columns` you want your badges to
be rendered in the output PDF.

### Colors

#### Color Definitions

You may define your own colors through the variable `color`, which must be
set to a list of objects defining three fields:

+ `name`: the name of the color you are defining.
+ `type`: the type of the color, among `gray`, `rgb`, `RGB`, `html`, `HTML`,
  etc.
+ `value`: the actual value of the color.

Note that each color is then automatically translated into LaTeX by using the
following command:

```latex
\definecolor{name}{type}{value}
```

#### Color Settings

Once you have defined your own colors, you may set the following properties
to customize the colors of your badges (of course, you may also use
predefined colors):

+ `name-color`: the color for the name of the participant.
+ `institute-color`: the color for the institute of the participant.
+ `event-color`: the color for the event in which the badges are worn.
+ `line-color`: the color for the footline.

By default, all colors are set to `black`.

## Building your Badges

Suppose that you have written the metadata of your badges in a file `input.md`.
Then, to produce these badges in a file named `output.pdf`, execute the
following command:

```bash
pandoc --template badges.pandoc input.md -o output.pdf
```
